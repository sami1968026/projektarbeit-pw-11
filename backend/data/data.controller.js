import axios from "axios";
// Holt von der API alle benötigten Daten des ausgewählten Jahres
async function getAll(request, res) {
  const jahr = request.params.jahr;
  const allData = [];
  const response = await axios.get(
    "https://data.tg.ch/api/v2/catalog/datasets/sk-stat-11/exports/json"
  );
  const userData = response.data;
  for (let i = 0; i < userData.length; i++) {
    if (userData[i].wahljahr === jahr) {
      allData.push({
        gemeinde_name: userData[i].gemeinde_name,
        wahlberechtigte_personen: userData[i].wahlberechtigte_personen,
        wahlbeteiligung_in_prozent: userData[i].wahlbeteiligung_in_prozent,
        wahljahr: userData[i].wahljahr,
        bfs_nr_gemeinde: userData[i].bfs_nr_gemeinde,
      });
    }
  }
  console.log(allData);
  res.json(allData);
}
// Holt die Wahlberechtigungen des ausgewählten Jahres und gibt den Array zurück
async function getWahlberechtigungen(request, res) {
  const jahr1 = request.params.jahr2;
  const wahlberechtigte_personen = [];
  const response = await axios.get(
    "https://data.tg.ch/api/v2/catalog/datasets/sk-stat-11/exports/json"
  );

  const userData = response.data;
  for (let i = 0; i < userData.length; i++) {
    if (userData[i].wahljahr === jahr1) {
      wahlberechtigte_personen.push({
        gemeinde_name: userData[i].gemeinde_name,
        wahlberechtigte_personen: userData[i].wahlberechtigte_personen,
        wahljahr: userData[i].wahljahr,
        bfs_nr_gemeinde: userData[i].bfs_nr_gemeinde,
      });
    }
  }
  console.log(wahlberechtigte_personen);
  res.json(wahlberechtigte_personen);
}
//Holt von der API die Wahlbeteiligungen des ausgewählten Jahres und speichert die benötigten Daten in einem Array
async function getWahlbeteiligungen(request, res) {
  const jahr2 = request.params.jahr3;
  const wahlbeteiligung_in_prozent = [];
  const response = await axios.get(
    "https://data.tg.ch/api/v2/catalog/datasets/sk-stat-11/exports/json"
  );

  const userData = response.data;
  for (let i = 0; i < userData.length; i++) {
    if (userData[i].wahljahr === jahr2) {
      wahlbeteiligung_in_prozent.push({
        gemeinde_name: userData[i].gemeinde_name,
        wahlbeteiligung_in_prozent: userData[i].wahlbeteiligung_in_prozent,
        wahljahr: userData[i].wahljahr,
        bfs_nr_gemeinde: userData[i].bfs_nr_gemeinde,
      });
    }
  }

  res.json(wahlbeteiligung_in_prozent);
}
export { getAll, getWahlberechtigungen, getWahlbeteiligungen };
