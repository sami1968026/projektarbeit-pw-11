import { Router } from "express";
import {
  getAll,
  getWahlbeteiligungen,
  getWahlberechtigungen,
} from "./data.controller.js";
const router = Router();
//Benötigte Schnittstellen mit den benötigten Parametern
router.get("/all/:jahr", getAll);
router.get("/wahlberechtigungen/:jahr2", getWahlberechtigungen);
router.get("/wahlbeteiligungen/:jahr3", getWahlbeteiligungen);

export { router };
