//Default typ für das ausgewählte Jahr
let jahr = 2020;

function changeid(Id) {
  jahr = Id;
  addHoverListeners();
}
//Fügt jedem svg-element eine hover funktion sowie die entsprechenden Daten im Tooltip ein
function addHoverListeners() {
  axios
    .get("http://localhost:3001/api/data/all/" + jahr)
    .then((res) => {
      processGet(res.data);
    })
    .catch((err) => {
      console.log("Error Noels Code", err);
    });
  function processGet(data) {
    console.log(data);
    const elTooltip = document.getElementById("tooltip-map");

    for (let y = 0; y < data.length; y++) {
      const bfs = document.getElementById(data[y].bfs_nr_gemeinde);
      //Eventlistener wird hinzugefügt und Tooltip Daten werden angespasst
      bfs.addEventListener("mouseover", (event) => {
        elTooltip.innerHTML =
          "Gemeinde: " +
          data[y].gemeinde_name +
          "<br>" +
          "Wahljahr: " +
          data[y].wahljahr +
          "<br>" +
          "Wahlbeteiligung: " +
          data[y].wahlbeteiligung_in_prozent +
          "<br>" +
          "Wahlberechtigung: " +
          data[y].wahlberechtigte_personen;

        elTooltip.classList.remove("do-not-display");
        elTooltip.style.top = `${event.pageY}px`;
        elTooltip.style.left = `${event.pageX}px`;
      });
      bfs.addEventListener("mouseout", (event) => {
        elTooltip.classList.add("do-not-display");
      });
    }
  }
}

addHoverListeners();
