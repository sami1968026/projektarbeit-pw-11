let barChart;
let jahr2 = 2020;
// Wird ein neues Jahr ausgewählt werden alle Diagramme destroyt und wieder neu refresht
function changeid2(Id) {
  jahr2 = Id;
  barChart.destroy();
  lineChart.destroy();
  barChart2.destroy();
  lineflopp.destroy();
  loadWahlberechtigte();
  loadWahlbeteiligung();
}
// Holt vom Backend die Wahlberechtigungen nach dem parameter jahr2
function loadWahlberechtigte() {
  axios
    .get("http://localhost:3001/api/data/wahlberechtigungen/" + jahr2)
    .then((res) => {
      console.log("Data", res.data);
      const topTen = getFirstTen(res.data);
      const flopTen = getflopTen(res.data);
    });
}
//Holt vom Backend die Wahlbeteiligungen nach dem parameter Jahr2
function loadWahlbeteiligung() {
  axios
    .get("http://localhost:3001/api/data/wahlbeteiligungen/" + jahr2)
    .then((res) => {
      console.log("Data", res.data);
      const topTen2 = getFirstTen2(res.data);
      const flopTen2 = getflopTen2(res.data);
    });
}
//Sortiert die Daten des ersten Bar Diagramms nach den Top 10 Wahlbeteiligungen
function getFirstTen(data) {
  data.sort((a, b) => b.wahlberechtigte_personen - a.wahlberechtigte_personen);
  const topTen = data.slice(0, 10);

  // Call the bar1() function and pass the top 10 data
  console.log(topTen);
  bar1(topTen);
}
//Sortiert die Daten des ersten Line Diagramms nach den Top 10 Wahlbeteiligungen
function getFirstTen2(data) {
  data.sort(
    (a, b) => b.wahlbeteiligung_in_prozent - a.wahlbeteiligung_in_prozent
  );
  const topTen = data.slice(0, 10);

  // Call the bar1() function and pass the top 10 data
  console.log(topTen);
  line1(topTen);
}
//Sortiert die Daten des zweiten Bar Diagramms nach den Flop 10 Wahlberechtigungen
function getflopTen(data) {
  data.sort((a, b) => a.wahlberechtigte_personen - b.wahlberechtigte_personen);
  const bottomTen = data.slice(0, 10);

  // Rufe die bar1() Funktion auf und übergebe die schlechtesten 10 Daten
  console.log(bottomTen);
  bar2(bottomTen);
}
//Sortiert die Daten des Line Diagramms nach den Top 10 Wahlbeteiligungen
function getflopTen2(data) {
  data.sort(
    (a, b) => a.wahlbeteiligung_in_prozent - b.wahlbeteiligung_in_prozent
  );
  const bottomTen = data.slice(0, 10);

  // Rufe die bar1() Funktion auf und übergebe die schlechtesten 10 Daten
  console.log(bottomTen);
  line2(bottomTen);
}

SVGInject.setOptions({ makeIdsUnique: false });
const elMapTg = document.getElementById("tg-map");
SVGInject(elMapTg);
// Erstellt das erste Bar Diagramm mit den Top 10 Wahlberechtigungen aus
function bar1(topTen) {
  console.log(topTen[0]);
  let label1 = "Wahlberechtigte";
  let chartobj = document.getElementById("bar-chart1");
  barChart = new Chart(chartobj, {
    type: "bar",
    data: {
      labels: [
        topTen[0].gemeinde_name,
        topTen[1].gemeinde_name,
        topTen[2].gemeinde_name,
        topTen[3].gemeinde_name,
        topTen[4].gemeinde_name,
        topTen[5].gemeinde_name,
        topTen[6].gemeinde_name,
        topTen[7].gemeinde_name,
        topTen[8].gemeinde_name,
        topTen[9].gemeinde_name,
      ],
      datasets: [
        {
          label: label1,
          data: [
            topTen[0].wahlberechtigte_personen,
            topTen[1].wahlberechtigte_personen,
            topTen[2].wahlberechtigte_personen,
            topTen[3].wahlberechtigte_personen,
            topTen[4].wahlberechtigte_personen,
            topTen[5].wahlberechtigte_personen,
            topTen[6].wahlberechtigte_personen,
            topTen[7].wahlberechtigte_personen,
            topTen[8].wahlberechtigte_personen,
            topTen[9].wahlberechtigte_personen,
          ],
        },
      ],
    },
  });
}
//Erstellt das zweite Bar Diagramm mit den Flop 10 Datensätzen der Wahlberechtigten
function bar2(bottomTen) {
  let label1 = "Wahlberechtigte";
  let chartobj = document.getElementById("bar-chart2");
  barChart2 = new Chart(chartobj, {
    type: "bar",
    data: {
      labels: [
        bottomTen[9].gemeinde_name,
        bottomTen[8].gemeinde_name,
        bottomTen[7].gemeinde_name,
        bottomTen[6].gemeinde_name,
        bottomTen[5].gemeinde_name,
        bottomTen[4].gemeinde_name,
        bottomTen[3].gemeinde_name,
        bottomTen[2].gemeinde_name,
        bottomTen[1].gemeinde_name,
        bottomTen[0].gemeinde_name,
      ],
      datasets: [
        {
          label: label1,
          data: [
            bottomTen[9].wahlberechtigte_personen,
            bottomTen[8].wahlberechtigte_personen,
            bottomTen[7].wahlberechtigte_personen,
            bottomTen[6].wahlberechtigte_personen,
            bottomTen[5].wahlberechtigte_personen,
            bottomTen[4].wahlberechtigte_personen,
            bottomTen[3].wahlberechtigte_personen,
            bottomTen[2].wahlberechtigte_personen,
            bottomTen[1].wahlberechtigte_personen,
            bottomTen[0].wahlberechtigte_personen,
          ],
        },
      ],
    },
  });
}
//Erstellt das erste Line Diagramm der Top 10 Wahlbeteiligungen in Prozent
function line1(topten) {
  let label1 = "Wahlbeteiligung in Prozent";
  let chartobj = document.getElementById("line-chart1");
  lineChart = new Chart(chartobj, {
    type: "line",
    data: {
      labels: [
        topten[0].gemeinde_name,
        topten[1].gemeinde_name,
        topten[2].gemeinde_name,
        topten[3].gemeinde_name,
        topten[4].gemeinde_name,
        topten[5].gemeinde_name,
        topten[6].gemeinde_name,
        topten[7].gemeinde_name,
        topten[8].gemeinde_name,
        topten[9].gemeinde_name,
      ],
      datasets: [
        {
          label: label1,
          data: [
            topten[0].wahlbeteiligung_in_prozent,
            topten[1].wahlbeteiligung_in_prozent,
            topten[2].wahlbeteiligung_in_prozent,
            topten[3].wahlbeteiligung_in_prozent,
            topten[4].wahlbeteiligung_in_prozent,
            topten[5].wahlbeteiligung_in_prozent,
            topten[6].wahlbeteiligung_in_prozent,
            topten[7].wahlbeteiligung_in_prozent,
            topten[8].wahlbeteiligung_in_prozent,
            topten[9].wahlbeteiligung_in_prozent,
          ],
        },
      ],
    },
  });
}
//Erstellt das zweite Line Diagramm mit den Flop 10 Wahlbeteiligungen in Prozent
function line2(bottomTen) {
  let label1 = "Wahlbeteiligung in Prozent";
  let chartobj = document.getElementById("line-chart2");
  lineflopp = new Chart(chartobj, {
    type: "line",
    data: {
      labels: [
        bottomTen[9].gemeinde_name,
        bottomTen[8].gemeinde_name,
        bottomTen[7].gemeinde_name,
        bottomTen[6].gemeinde_name,
        bottomTen[5].gemeinde_name,
        bottomTen[4].gemeinde_name,
        bottomTen[3].gemeinde_name,
        bottomTen[2].gemeinde_name,
        bottomTen[1].gemeinde_name,
        bottomTen[0].gemeinde_name,
      ],
      datasets: [
        {
          label: label1,
          data: [
            bottomTen[9].wahlbeteiligung_in_prozent,
            bottomTen[8].wahlbeteiligung_in_prozent,
            bottomTen[7].wahlbeteiligung_in_prozent,
            bottomTen[6].wahlbeteiligung_in_prozent,
            bottomTen[5].wahlbeteiligung_in_prozent,
            bottomTen[4].wahlbeteiligung_in_prozent,
            bottomTen[3].wahlbeteiligung_in_prozent,
            bottomTen[2].wahlbeteiligung_in_prozent,
            bottomTen[1].wahlbeteiligung_in_prozent,
            bottomTen[0].wahlbeteiligung_in_prozent,
          ],
        },
      ],
    },
  });
}

loadWahlberechtigte();
loadWahlbeteiligung();
