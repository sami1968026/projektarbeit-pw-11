import express from "express";
import { router as dataRouter } from "./backend/data/data.routes.js";
import cors from "cors";

const app = express();
app.use(cors());
app.use("/api/data", dataRouter);

app.use(express.static("Frontend"));

app.use(express.json());

app.listen(3001, () => {
  console.log("Server listens to http://localhost:3001");
});
